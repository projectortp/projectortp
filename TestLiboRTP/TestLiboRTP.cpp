// TestLiboRTP.cpp : Defines the entry point for the application.
//

#include "stdafx.h"
#include "TestLiboRTP.h"

#include <iostream>
#include <thread>

#include <ortp/ortp.h>


#define STREAMS_COUNT 1000

enum
{
	EVENT_STOP,
	EVENT_RTP,
	EVENT_COUNT						//  Always last
};

//#define EXAMPLE_PORT 6000
//#define EXAMPLE_GROUP "239.0.0.1"
#define EXAMPLE_GROUP "224.5.6.7"
//#define EXAMPLE_GROUP "192.168.26.10"
#define EXAMPLE_PORT 5700

//sicemal writeExactMulticast
//static struct sockaddr_in addr;
//static int addrlen, cnt;
//static sock = NULL;


static RtpSession *	m_Session[STREAMS_COUNT];
static int				m_nPacket_Size = 1024;
static int				m_nTimestamp_Inc = 1024;
static char		*	m_pBuffer = NULL;
static char		*	m_SSRC = NULL;
static int				m_nChannels = 1;
static int				m_nPort = 0;
static HANDLE			m_hEvents[EVENT_COUNT];
static BOOL			m_bExit = FALSE;
static char *help = "usage: mrtpsend	filename ip port nstreams [--packet-size size] [--ts-inc value]\n";

void hello(){
	std::cout << "Hello from thread " << std::endl;
}

void
SendMulticast(/*rfbClientPtr cl*/)
{
	static int sequencia = 0;

	//if (cl->screen->multicastPacketLenght == 0) return;

	//cl->screen->multicastUpdateBuffer = TRUE;
	int block = m_nPacket_Size;

	static SessionSet	*	pSessionSet = NULL;
	int				nCounter = 0;
	UINT32			m_nUser_Timestamp = 0;

	char* lbuf;
	lbuf = (char *)malloc(m_nPacket_Size);

	printf("==> Starting the RTP Sender test\n");
	printf("==> Timestamp increment will be %i\n", m_nTimestamp_Inc);
	printf("==> Packet size will be %i\n", m_nPacket_Size);

	m_pBuffer = (char *)ortp_malloc(m_nPacket_Size);


	if (pSessionSet == NULL)
	{
		ortp_init();
		ortp_scheduler_init();
		printf("==> Scheduler initialized\n");

		m_SSRC = "SSRC";
		m_nPort = EXAMPLE_PORT;

		for (nCounter = 0; nCounter < m_nChannels; nCounter++)
		{
			m_Session[nCounter] = rtp_session_new(RTP_SESSION_SENDONLY);

			rtp_session_set_scheduling_mode(m_Session[nCounter], 1);
			rtp_session_set_blocking_mode(m_Session[nCounter], 0);
			rtp_session_set_remote_addr(m_Session[nCounter], EXAMPLE_GROUP, m_nPort);
			rtp_session_set_send_payload_type(m_Session[nCounter], 0);

			if (m_SSRC != NULL)
			{
				rtp_session_set_ssrc(m_Session[nCounter], atoi(m_SSRC));
			}

			m_nPort += 2;
		}

		pSessionSet = session_set_new();
	}

	int t = 0;
	char* buf;
	buf = (char *)malloc(block + 7);

	int r = 0;
	sequencia &= 0xffff;
	sequencia++;

	printf("##> Sequencia: %d\n", sequencia);



	int i = 0;
	//t = (cl->screen->multicastPacketLenght / block);

	char buffer[1024] = "TESTE DE TRANSMISSAO RTP";
	int multicastPacketLenght = 1024;

	int pacote = 0;
	while (i < 10240) //cl->screen->multicastPacketLenght)
	{


		//memcpy(buf, cl->screen->multicastBuffer + i, block);
		memcpy(buf, buffer + i, block);

		int k;
		for (k = 0; k<m_nChannels; k++){
			session_set_set(pSessionSet, m_Session[k]);
		}
		session_set_select(NULL, pSessionSet, NULL);

		for (k = 0; k<m_nChannels; k++)
		{
			if (session_set_is_set(pSessionSet, m_Session[k]))
			{

				rtp_session_send_with_ts(m_Session[k], (uint8_t*)buf, block, m_nUser_Timestamp * 10);
			}
		}
		m_nUser_Timestamp += m_nTimestamp_Inc;

		i += block;
	}

	//int pad = i - cl->screen->multicastPacketLenght;
	int pad = i - multicastPacketLenght;


	//memcpy(buf, cl->screen->multicastBuffer, block);
	memcpy(buf, buffer, block);

	int k;
	for (k = 0; k<m_nChannels; k++){
		session_set_set(pSessionSet, m_Session[k]);
	}
	session_set_select(NULL, pSessionSet, NULL);
	for (k = 0; k<m_nChannels; k++)
	{
		if (session_set_is_set(pSessionSet, m_Session[k]))
		{
			//rtp_session_send_with_ts(m_Session[k],(uint8_t*)buf,block+6,m_nUser_Timestamp);
			rtp_session_send_with_ts(m_Session[k], (uint8_t*)buf, block, m_nUser_Timestamp * 10);
		}
	}
	m_nUser_Timestamp += m_nTimestamp_Inc;


	//cl->screen->multicastUpdateBuffer = FALSE;

	//cl->screen->multicastPacketLenght = 0;

}


#define MAX_LOADSTRING 100

// Global Variables:
HINSTANCE hInst;								// current instance
TCHAR szTitle[MAX_LOADSTRING];					// The title bar text
TCHAR szWindowClass[MAX_LOADSTRING];			// the main window class name

// Forward declarations of functions included in this code module:
ATOM				MyRegisterClass(HINSTANCE hInstance);
BOOL				InitInstance(HINSTANCE, int);
LRESULT CALLBACK	WndProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK	About(HWND, UINT, WPARAM, LPARAM);

int APIENTRY _tWinMain(_In_ HINSTANCE hInstance,
                     _In_opt_ HINSTANCE hPrevInstance,
                     _In_ LPTSTR    lpCmdLine,
                     _In_ int       nCmdShow)
{
	UNREFERENCED_PARAMETER(hPrevInstance);
	UNREFERENCED_PARAMETER(lpCmdLine);

 	// TODO: Place code here.
	MSG msg;
	HACCEL hAccelTable;

	// Initialize global strings
	LoadString(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
	LoadString(hInstance, IDC_TESTLIBORTP, szWindowClass, MAX_LOADSTRING);
	MyRegisterClass(hInstance);

	// Perform application initialization:
	if (!InitInstance (hInstance, nCmdShow))
	{
		return FALSE;
	}

	hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_TESTLIBORTP));

	std::thread t1(hello);
	t1.join();

	char* lbuf;
	lbuf = (char *)malloc(m_nPacket_Size + 6);
	m_pBuffer = (char *)ortp_malloc(m_nPacket_Size);

	//ortp_init();
	SendMulticast();

	// Main message loop:
	while (GetMessage(&msg, NULL, 0, 0))
	{
		if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
	}

	return (int) msg.wParam;
}



//
//  FUNCTION: MyRegisterClass()
//
//  PURPOSE: Registers the window class.
//
ATOM MyRegisterClass(HINSTANCE hInstance)
{
	WNDCLASSEX wcex;

	wcex.cbSize = sizeof(WNDCLASSEX);

	wcex.style			= CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc	= WndProc;
	wcex.cbClsExtra		= 0;
	wcex.cbWndExtra		= 0;
	wcex.hInstance		= hInstance;
	wcex.hIcon			= LoadIcon(hInstance, MAKEINTRESOURCE(IDI_TESTLIBORTP));
	wcex.hCursor		= LoadCursor(NULL, IDC_ARROW);
	wcex.hbrBackground	= (HBRUSH)(COLOR_WINDOW+1);
	wcex.lpszMenuName	= MAKEINTRESOURCE(IDC_TESTLIBORTP);
	wcex.lpszClassName	= szWindowClass;
	wcex.hIconSm		= LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));

	return RegisterClassEx(&wcex);
}

//
//   FUNCTION: InitInstance(HINSTANCE, int)
//
//   PURPOSE: Saves instance handle and creates main window
//
//   COMMENTS:
//
//        In this function, we save the instance handle in a global variable and
//        create and display the main program window.
//
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
   HWND hWnd;

   hInst = hInstance; // Store instance handle in our global variable

   hWnd = CreateWindow(szWindowClass, szTitle, WS_OVERLAPPEDWINDOW,
      CW_USEDEFAULT, 0, CW_USEDEFAULT, 0, NULL, NULL, hInstance, NULL);

   if (!hWnd)
   {
      return FALSE;
   }

   ShowWindow(hWnd, nCmdShow);
   UpdateWindow(hWnd);

   return TRUE;
}

//
//  FUNCTION: WndProc(HWND, UINT, WPARAM, LPARAM)
//
//  PURPOSE:  Processes messages for the main window.
//
//  WM_COMMAND	- process the application menu
//  WM_PAINT	- Paint the main window
//  WM_DESTROY	- post a quit message and return
//
//
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	int wmId, wmEvent;
	PAINTSTRUCT ps;
	HDC hdc;

	switch (message)
	{
	case WM_COMMAND:
		wmId    = LOWORD(wParam);
		wmEvent = HIWORD(wParam);
		// Parse the menu selections:
		switch (wmId)
		{
		case IDM_ABOUT:
			DialogBox(hInst, MAKEINTRESOURCE(IDD_ABOUTBOX), hWnd, About);
			break;
		case IDM_EXIT:
			DestroyWindow(hWnd);
			break;
		default:
			return DefWindowProc(hWnd, message, wParam, lParam);
		}
		break;
	case WM_PAINT:
		hdc = BeginPaint(hWnd, &ps);
		// TODO: Add any drawing code here...
		EndPaint(hWnd, &ps);
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	default:
		return DefWindowProc(hWnd, message, wParam, lParam);
	}
	return 0;
}

// Message handler for about box.
INT_PTR CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	UNREFERENCED_PARAMETER(lParam);
	switch (message)
	{
	case WM_INITDIALOG:
		return (INT_PTR)TRUE;

	case WM_COMMAND:
		if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
		{
			EndDialog(hDlg, LOWORD(wParam));
			return (INT_PTR)TRUE;
		}
		break;
	}
	return (INT_PTR)FALSE;
}
